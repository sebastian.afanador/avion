
package avion;

public class AvionDeGuerra extends Avion{
    private Boolean enServicio;
    private Bomba bomba;

    public AvionDeGuerra(Boolean enServicio, Bomba bomba, int numeroDePasajeros, String marcaAvion) {
        super(numeroDePasajeros, marcaAvion);
        this.enServicio = enServicio;
        this.bomba = bomba;
    }

    public Boolean getEnServicio() {
        return enServicio;
    }

    public void setEnServicio(Boolean enServicio) {
        this.enServicio = enServicio;
    }

    public Bomba getBomba() {
        return bomba;
    }

    public void cargarBomba(Bomba bomba) {
        this.bomba = bomba;
    }
    
    
       
    
    
}
