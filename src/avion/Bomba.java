
package avion;

public class Bomba {
    private int dano;

    public Bomba(int dano) {
        this.dano = dano;
    }

    public int getDano() {
        return dano;
    }

    public void setDano(int dano) {
        this.dano = dano;
    }
    
    public static void main(String[] args) {
        Bomba bomba1 = new Bomba(1000);
        Bomba bomba2 = new Bomba(200);
        Bomba bomba3 = new Bomba(300);
        
        AvionDeGuerra jet = new AvionDeGuerra(true, bomba1, 2, "Boing");
        System.out.println("El daño de la bomba es: "+jet.getBomba().getDano());
        
        jet.cargarBomba(bomba2);
        System.out.println("El daño de la bomba es: "+jet.getBomba().getDano());
        
        jet.cargarBomba(bomba3);
        System.out.println("El daño de la bomba es: "+jet.getBomba().getDano());
        
        
        
        
    }
    
}
