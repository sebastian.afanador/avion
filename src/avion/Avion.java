
package avion;

public class Avion {
    private int numeroDePasajeros;
    private String marcaAvion;

    public Avion(int numeroDePasajeros, String marcaAvion) {
        this.numeroDePasajeros = numeroDePasajeros;
        this.marcaAvion = marcaAvion;
    }

    public int getNumeroDePasajeros() {
        return numeroDePasajeros;
    }

    public void setNumeroDePasajeros(int numeroDePasajeros) {
        this.numeroDePasajeros = numeroDePasajeros;
    }

    public String getMarcaAvion() {
        return marcaAvion;
    }

    public void setMarcaAvion(String marcaAvion) {
        this.marcaAvion = marcaAvion;
    }
    
    
    
    
}
